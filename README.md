tictactoe
=========
To start the app just use following command in terminal

php app/console server:start


Complexity:
===========
Without using alpha beta cutting 

Performance complexity will be 
    O(b power m) where m is the depth and b is available moves
    
Space complexity will be
    O(bm)
    

Using alpha beta cutting 

Performance complexity will be 
    O(b power m/2) where m is the depth and b is available moves