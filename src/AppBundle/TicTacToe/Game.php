<?php
namespace AppBundle\TicTacToe;

/**
 * Created by PhpStorm.
 * User: muhammadshaker
 * Date: 13.10.17
 * Time: 13:28
 */
class Game
{
    private $boardState;
    private $currentPlayer;
    /**
     * Player X (Human) will always take the first move
    */
    const PLAYER_X = 'X';
    const PLAYER_O = 'O';


    private function validateState($boardState){
        if(!is_array($boardState) || count($boardState) != 3 ||
            !is_array($boardState[0]) || count($boardState[0]) != 3 ||
            !is_array($boardState[1]) || count($boardState[1]) != 3 ||
            !is_array($boardState[2]) || count($boardState[2]) != 3)
        {
            throw new \InvalidArgumentException();
        }

        $movesO = 0;
        $movesX = 0;
        foreach($boardState as $row){
            foreach($row as $cel){
                if($cel !== self::PLAYER_O && $cel !== self::PLAYER_X && $cel !== '')
                    throw new \InvalidArgumentException();
                if($cel === self::PLAYER_O)
                    $movesO++;
                if($cel === self::PLAYER_X)
                    $movesX++;
            }
        }
        if(abs($movesX-$movesO) > 1)
            throw new \InvalidArgumentException();
    }

    public function __construct($boardState)
    {
        $this->validateState($boardState);
        $this->boardState = $boardState;
        $this->currentPlayer = $this->nextMovePlayer();
    }

    public function getCurrentPlayer(){
        return $this->currentPlayer;
    }

    public function nextMovePlayer(){
        $movesO = 0;
        $movesX = 0;
        foreach($this->boardState as $row){
            foreach($row as $cel){
                if($cel === self::PLAYER_O)
                    $movesO++;
                if($cel === self::PLAYER_X)
                    $movesX++;
            }
        }

        if($movesX+$movesO == 9) return null;

        return $movesX-$movesO==1?self::PLAYER_O:self::PLAYER_X;
    }

    public function availableMoves()
    {
        $availableMoves = [];
        $rowIndex = 0;
        foreach($this->boardState as $row){
            $colIndex = 0;
            foreach($row as $cel){
                if($cel === '')
                    array_push($availableMoves,new Move($rowIndex, $colIndex));
                $colIndex++;
            }
            $rowIndex++;
        }
        return $availableMoves;
    }



    public function winner($player){
        /** X X X */
        if($this->boardState[0][0] == $player && $this->boardState[0][1] == $player && $this->boardState[0][2] == $player)
            return true;
        if($this->boardState[1][0] == $player && $this->boardState[1][1] == $player && $this->boardState[1][2] == $player)
            return true;
        if($this->boardState[2][0] == $player && $this->boardState[2][1] == $player && $this->boardState[2][2] == $player)
            return true;

        /** X
         *  X
         *  X  */
        if($this->boardState[0][0] == $player && $this->boardState[1][0] == $player && $this->boardState[2][0] == $player)
            return true;
        if($this->boardState[0][1] == $player && $this->boardState[1][1] == $player && $this->boardState[2][1] == $player)
            return true;
        if($this->boardState[0][2] == $player && $this->boardState[1][2] == $player && $this->boardState[2][2] == $player)
            return true;

        /** X
         *    X
         *      X  */
        if($this->boardState[0][0] == $player && $this->boardState[1][1] == $player && $this->boardState[2][2] == $player)
            return true;

        /**     X
         *    X
         *  X     */
        if($this->boardState[0][2] == $player && $this->boardState[1][1] == $player && $this->boardState[2][0] == $player)
            return true;
        return false;
    }

    public function over(){
        return ($this->winner(self::PLAYER_O) || $this->winner(self::PLAYER_X) || count($this->availableMoves()) == 0);
    }


    public function result($player = null){
        $player = $player?:$this->getCurrentPlayer();
        if(count($this->availableMoves()) == 0){
            return 0;
        }else if($this->winner($player)){
            return 1;
        }else{
            return -1;
        }
    }

    public function move(Move $move = null)
    {
        if($this->over())
            throw new \InvalidArgumentException();

        $player = $this->nextMovePlayer();
        $result = null;
        if($move == null){
            $move = $this->minimax($player);
            $this->boardState[$move->getRow()][$move->getCol()] = $player;
        }else{
            if(!in_array($move, $this->availableMoves()))
                throw new \InvalidArgumentException();

            $this->boardState[$move->getRow()][$move->getCol()] = $player;
        }
        return $move;
    }

    public function minimax($player){
        $min = -2;
        $bestMove = null;
        foreach ($this->availableMoves() as $move) {
            $tempBoardState = $this->boardState;
            $tempBoardState[$move->getRow()][$move->getCol()] = $this->currentPlayer;
            $game = new Game($tempBoardState);
            $result = $game->minValue($player, -2, 2);
            if($result >= $min){
                $bestMove = $move;
                $min = $result;
            }
        }
        return $bestMove;
    }

    /**
     * @param $player
     * @param $alpha
     * @param $beta
     * @return int
     */
    public function maxValue($player,$alpha, $beta){
        if($this->over()){
            return $this->result($player);
        }else{
            $value = -2;
            /** @var Move $move */
            foreach ($this->availableMoves() as $move){
                $tempBoardState = $this->boardState;
                $tempBoardState[$move->getRow()][$move->getCol()] = $this->currentPlayer;
                $game = new Game($tempBoardState);

                $value = max($value, $game->minValue($player, $alpha, $beta));
                if($value >= $beta) return $value;
                $alpha = max($alpha, $value);
            }
            return $value;
        }
    }

    /**
     * @param $player
     * @param $alpha
     * @param $beta
     * @return int
     */
    public function minValue($player,$alpha, $beta){
        if($this->over()){
            return $this->result($player);
        }else{
            $value = 2;
            /** @var Move $move */
            foreach ($this->availableMoves() as $move){
                $tempBoardState = $this->boardState;
                $tempBoardState[$move->getRow()][$move->getCol()] = $this->currentPlayer;
                $game = new Game($tempBoardState);

                $value = min($value, $game->maxValue($player, $alpha, $beta));
                if($value <= $alpha) return $value;
                $beta = min($beta, $value);
            }
            return $value;
        }
    }
}

