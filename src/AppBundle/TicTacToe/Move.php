<?php
/**
 * Created by PhpStorm.
 * User: muhammadshaker
 * Date: 13.10.17
 * Time: 13:48
 */

namespace AppBundle\TicTacToe;


class Move
{
    private $col;
    private $row;
    public $score;

    public function getCol()
    {
        return $this->col;
    }

    public function getRow(){
        return $this->row;
    }

    public function __construct($row, $col)
    {
        if($col < 0 || $col > 2 ||
            $row < 0 || $row > 2)
            throw new \InvalidArgumentException();

        $this->col = $col;
        $this->row = $row;
    }

}