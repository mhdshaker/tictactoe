<?php
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: muhammadshaker
 * Date: 14.10.17
 * Time: 16:29
 */

class GameTest extends TestCase
{
    /** @var AppBundle\TicTacToe\Game $game */
    private $game;
    public function setUp()
    {
        $this->game = new AppBundle\TicTacToe\Game([['O','','X'],['X','','X'],['','O','O']]);
    }

    public function testAvailableMoves(){
        $this->assertNotEquals($this->game->availableMoves(), 3, 'Incorrect number of moves!') ;
    }

    public function testWinner(){
        $this->game = new AppBundle\TicTacToe\Game([['X','X','X'],['O','','O'],['','','']]);
        $this->assertTrue($this->game->winner(\AppBundle\TicTacToe\Game::PLAYER_X), 'Invalid result!') ;

        $this->game = new AppBundle\TicTacToe\Game([['O','','O'],['X','X','X'],['','','']]);
        $this->assertTrue($this->game->winner(\AppBundle\TicTacToe\Game::PLAYER_X), 'Invalid result!') ;

        $this->game = new AppBundle\TicTacToe\Game([['O','','O'],['','',''],['X','X','X']]);
        $this->assertTrue($this->game->winner(\AppBundle\TicTacToe\Game::PLAYER_X), 'Invalid result!') ;
    }

    public function testOver(){
        $this->game = new AppBundle\TicTacToe\Game([['X','O','X'],['X','O','X'],['O','X','O']]);
        $this->assertTrue($this->game->over(\AppBundle\TicTacToe\Game::PLAYER_X), 'Invalid result!') ;
    }

}