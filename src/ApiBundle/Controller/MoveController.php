<?php
namespace ApiBundle\Controller;
use AppBundle\TicTacToe\Game;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: muhammadshaker
 * Date: 14.10.17
 * Time: 21:42
 */
class MoveController extends FOSRestController implements MoveInterface
{

    public function makeMove($boardState, $playerUnit = 'X', &$move = null)
    {
        $game = new \AppBundle\TicTacToe\Game($boardState);
        $move = $game->move($move);

        return $game;
    }

    // "post_move" [POST] /moves
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="This is api to play next move",
     *  parameters={
     *      {"name"="boardState", "dataType"="array", "required"=true, "description"="current state"},
     *      {"name"="move", "dataType"="string", "required"=false, "description"="next move"},
     *  }
     * )
     */
    public function postMovesAction(Request $request)
    {
        $player = $request->get('player');
        $move = $request->get('move');
        $boardState = $request->get('boardState');
        $boardState = json_decode($boardState);
        if($move != null){
            $moveIndexes = explode(".",$move);
            $move = new \AppBundle\TicTacToe\Move($moveIndexes[0], $moveIndexes[1]);
        }

        $game = $this->makeMove($boardState, $player, $move);

        $result = ['moveIndex' => "{$move->getRow()}.{$move->getCol()}"];
        if($game->over()){
            $result['gameover'] = $game->result(Game::PLAYER_X);
        }
        return $result;
    }
}