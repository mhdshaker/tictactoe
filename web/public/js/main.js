function apiPlay(boardState, player, move, success_callback, fail_callback) {
    $.ajax({
        url: '/api/moves',
        type: 'POST',
        data:{move:move, player:player, boardState:JSON.stringify(boardState)},
        success: function(result) {
            if(success_callback) success_callback(result.moveIndex, result.gameover);
        },
        error:function (result) {
            if(fail_callback) fail_callback();
            alert(JSON.parse(result.responseText).message);
        }

    });
}